/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    StyleSheet,
} from "react-native";
import MapView from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

export default class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            initialRegion: {
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }
        };
    }

    componentDidMount() {
        this.getCurrentLocation();
        this.props.navigation.addListener('focus', () => {
            this.getCurrentLocation();
        })
    }

    getCurrentLocation() {
        Geolocation.getCurrentPosition(
            position => {
                let region = {
                    latitude: parseFloat(position.coords.latitude),
                    longitude: parseFloat(position.coords.longitude),
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421
                };
                this.setState({
                    initialRegion: region
                });
            },
            error => console.log(error),
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    initialRegion={this.state.initialRegion}
                    showsUserLocation={true}
                    zoomEnabled={true}
                    onMapReady={this.onMapReady}
                    onRegionChangeComplete={this.onRegionChange}>

                    <MapView.Marker
                        coordinate={{
                            "latitude": this.state.initialRegion.latitude,
                            "longitude": this.state.initialRegion.longitude
                        }}
                        title={"Your Location"}
                        draggable />
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject
    }
});
