/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View, Image,
    StyleSheet
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";
import ComponentsStyles from "../../constants/Component.styles";
import OutlineButton from "../subComponents/OutlineButton";

export default class SelectWorkType extends Component {

    render() {
        return (
            <View style={styles.CONTAINER}>

                <View style={{ alignItems: 'center' }}>
                    <OutlineButton name="Start Tracking Your Team"
                        customStyle={styles.outline}
                        customTextStyle={styles.text}
                        onPress={() => this.props.navigation.navigate('DrawerHome')} />

                    <View style={{ padding: 10 }} />

                    <OutlineButton name="Join With Other Teams"
                        customStyle={[styles.outline, {
                            padding: 0, paddingTop: 30, paddingBottom: 30,
                            paddingLeft: 45, paddingRight: 45,
                        }]}
                        customTextStyle={styles.text}
                        onPress={() => this.props.navigation.navigate('DrawerHome')} />
                </View>

                <Image resizeMode={'stretch'} style={[styles.image, { top: 0 }]} source={require('../../assets/images/1.png')} />
                <Image resizeMode={'stretch'} style={[styles.image, { top: 0, alignSelf: "flex-end" }]} source={require('../../assets/images/2.png')} />
                <Image resizeMode={'stretch'} style={[styles.image, { bottom: 0 }]} source={require('../../assets/images/3.png')} />
                <Image resizeMode={'stretch'} style={[styles.image, { bottom: 0, alignSelf: "flex-end" }]} source={require('../../assets/images/4.png')} />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    CONTAINER: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    image: {
        alignSelf: 'flex-start', width: 120, height: 120, position: 'absolute'
    },

    outline: {
        borderColor: ComponentStyles.COLORS.LIGHT_BLUE,
        borderWidth: 2, borderRadius: 10,
        justifyContent: 'flex-start',
        padding: 30,
    },
    text: {
        color: ComponentsStyles.COLORS.BLUE,
        fontSize: 20,
        fontFamily: ComponentsStyles.FONT_FAMILY.BOLD
    },

    round: {
        backgroundColor: '#E0E0E0',
        borderRadius: 6,
        width: 6,
        height: 6,
    }
});