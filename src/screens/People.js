/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";
import { Agenda } from 'react-native-calendars';
import ComponentStyles from "../../constants/Component.styles";
import Arrays from "../../constants/JSONData";
import Header from "../components/Header";

export default class People extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items: {

            }
        }
    }

    loadItems(day) {
        setTimeout(() => {
            // for (let i = -10; i < 10; i++) {
            //     const time = day.timestamp + i * 24 * 60 * 60 * 1000;
            //     const strTime = this.timeToString(time);
            //     if (!this.state.items[strTime]) {
            //         this.state.items[strTime] = [];
            //         const numItems = Math.floor(Math.random() * 5);
            //         for (let j = 0; j < 100; j++) {
            //             this.state.items[strTime].push({
            //                 name: 'Kevin Jonas',
            //                 time: '9.30am - 11.30am',
            //                 capacity: '50',
            //             });
            //         }
            //     }
            // }
            //console.log(this.state.items);
            const newItems = {};
            Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
            this.setState({
                items: newItems
            });
        }, 1000);
        // console.log(`Load Items for ${day.year}-${day.month}`);
    }

    renderItem(item) {
        return (
            <View style={[styles.item]}>
                <Text style={{
                    fontFamily: ComponentStyles.FONT_FAMILY.BOLD,
                    color: ComponentStyles.COLORS.BLUE, fontSize: 12
                }}>{item.company}</Text>
                <Text style={{
                    fontFamily: ComponentStyles.FONT_FAMILY.BOLD,
                    color: ComponentStyles.COLORS.DARKER_GRAY, fontSize: 18
                }}>{item.task}</Text>
                <Text style={{
                    fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, bottom: 5,
                    color: ComponentStyles.COLORS.DARKER_GRAY, fontSize: 15
                }}>{item.address}</Text>
                <Text style={{
                    fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM,
                    color: ComponentStyles.COLORS.GREEN, fontSize: 16
                }}>{item.time}</Text>

            </View>
        );
    }

    renderEmptyDate() {
        return (
            <View style={{ flex: 1 }}></View>
        );
    }

    rowHasChanged(r1, r2) { return r1.name !== r2.name }

    // timeToString(time) {
    //     const date = new Date(time);
    //     return date.toISOString().split('T')[0];
    // }


    render() {
        return (
            <View style={ComponentStyles.CONTAINER}>
                {/* <View style={{
                    position: 'absolute', width: '100%',
                     height: '50%', backgroundColor: ComponentStyles.COLORS.THEME_BLUE,
                    borderBottomLeftRadius: 75, borderBottomRightRadius: 75,
                }} />
                <Header /> */}

                <Agenda
                    items={Arrays.Agenda}
                    loadItemsForMonth={this.loadItems.bind(this)}
                    selected={new Date()}
                    renderItem={this.renderItem.bind(this)}
                    renderEmptyDate={this.renderEmptyDate.bind(this)}
                    rowHasChanged={this.rowHasChanged.bind(this)}
                    // markingType={'period'}
                    // markedDates={{
                    //     '2020-09-18': { textColor: '#666' },
                    //     '2020-09-19': { textColor: '#666' },
                    //     '2020-09-20': { startingDay: true, endingDay: true, color: 'blue' },
                    //     '2020-09-21': { startingDay: true, color: 'blue' },
                    //     '2020-09-22': { endingDay: true, color: 'gray' },
                    //     '2020-09-24': { startingDay: true, color: 'gray' },
                    //     '2020-09-25': { color: 'gray' },
                    //     '2020-09-26': { endingDay: true, color: 'gray' }
                    // }}
                    monthFormat={'MMMM yyyy'}
                    theme={{ calendarBackground: 'white', agendaKnobColor: ComponentStyles.COLORS.BLUE }}
                // renderDay={(day, item) => (<Text>{day ? day.day : 'item'}</Text>)}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: ComponentStyles.COLORS.WHITE, marginRight: 15, marginTop: 20,
        flex: 1, elevation: 2, padding: 10, borderRadius: 8
    }
});