/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    Switch,
    TouchableOpacity
} from "react-native";
import ComponentsStyles from "../../constants/Component.styles";
import InputText from "../components/InputText";
import ActionButton from "../components/ActionButton";
import ComponentStyles from "../../constants/Component.styles";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            origin: 'signup'
        };
    }


    render() {
        const origin = this.state.origin;
        return (
            <View style={[styles.CONTAINER, { backgroundColor: ComponentStyles.COLORS.THEME_BLUE }]}>
                <View style={{ width: '85%', marginTop: 30, }}>
                    <Text style={{ fontFamily: ComponentStyles.FONT_FAMILY.BOLD, color: 'white', fontSize: 25 }}>Sign In</Text>
                    <Text style={{ fontFamily: ComponentStyles.FONT_FAMILY.REGULAR, fontSize: 15, color: 'white' }}>Enter your registerd mobile number and password for sign in.</Text>

                    <View style={{ height: 10, }} />
                    <InputText style={styles.input} placeholder='Mobile Number' placeholderColor='black' />
                    <View style={{ height: 10, }} />
                    <InputText style={styles.input} placeholder='Password' placeholderColor='black' secureTextEntry={true} />
                    <View style={{ height: 10, }} />

                    <ActionButton
                        customBtnStyle={{ backgroundColor: ComponentStyles.COLORS.BLUE }}
                        customTextStyle={{ fontSize: 20 }}
                        title='Sign in'
                        onPress={() => this.props.navigation.navigate('SelectWorkType')} />

                    <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }}>
                        <Text style={[{
                            fontFamily: ComponentStyles.FONT_FAMILY.REGULAR,
                            color: 'white', marginRight: 6, fontSize: 13
                        }]}>Forgot Password?</Text>
                        <TouchableOpacity>
                            <Text style={{
                                fontFamily: ComponentStyles.FONT_FAMILY.REGULAR,
                                color: '#33cfff', fontSize: 12
                            }}> Reset here </Text>
                        </TouchableOpacity>

                    </View>

                </View>

                <View style={styles.regBox}>
                    <Text style={[{
                        fontSize: 13,
                        fontFamily: ComponentStyles.FONT_FAMILY.REGULAR, color: 'white', marginRight: 6
                    }]}>  Don't have an account? </Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('EditUserDetails', { origin: origin })}>
                        <Text style={{
                            fontSize: 12,
                            color: '#33cfff', fontFamily: ComponentStyles.FONT_FAMILY.REGULAR
                        }}>Signup now</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ height: 25, }} />
            </View >
        );
    }
}



const styles = StyleSheet.create({
    CONTAINER: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    regBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 6
    },

    input: {
        backgroundColor: '#E4E4E4',
        opacity: 0.5,
        fontFamily: ComponentsStyles.FONT_FAMILY.MEDIUM,
        fontSize: 14, paddingBottom: 0,
    }
});