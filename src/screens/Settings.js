/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    FlatList
} from "react-native";
import { StackActions } from '@react-navigation/native';
import ComponentStyles from "../../constants/Component.styles";
import ActionButton from "../components/ActionButton";
import InputText from "../components/InputText";
import SubHeader from "../subComponents/SubHeader";
import ToggleSwitch from "../subComponents/ToggleSwitch";
import Arrays from '../../constants/JSONData'

export default class Settings extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpenForRequests: false,
            isDutyOnForCompanies: false,
            companyList: [],
        }
    }

    componentDidMount() {
        let tempArray = Arrays.RegisteredCompanyList.map((item) => {
            item.isSelectedCompany = false;
            return { ...item };
        });
        this.setState({ companyList: tempArray });
        // console.log('arr data==>', tempArray);

    }

    onToggle = (position) => {
        const { companyList } = this.state;
        let tempUpdatedArray = companyList.map((item, index) => {
            position == index ? item.isSelectedCompany = !item.isSelectedCompany : null
            return { ...item };
        })
        this.setState({ companyList: tempUpdatedArray });
        // console.log('Selection data==>', tempUpdatedArray);

    }

    render() {
        const { isOpenForRequests, isDutyOnForCompanies } = this.state;
        return (
            <View style={ComponentStyles.CONTAINER}>
                <SubHeader title={'Settings'} navigation={() => this.props.navigation.goBack()} />
                <View style={ComponentStyles.CONTENT}>

                    <ToggleSwitch
                        value={isOpenForRequests}
                        title={'Open for Requests'}
                        mainCategory={true}
                        onValueChange={() => { this.setState({ isOpenForRequests: !isOpenForRequests }) }}
                    />
                    {/* <Text style={[styles.title, this.props.customTextStyle]}>{this.props.title}</Text> */}

                    <ToggleSwitch
                        value={isDutyOnForCompanies}
                        title={'Duty on for Companies'}
                        mainCategory={true}
                        onValueChange={() => { this.setState({ isDutyOnForCompanies: !isDutyOnForCompanies }) }}
                    />

                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        data={this.state.companyList}
                        renderItem={({ item, index }) => {
                            return (
                                <ToggleSwitch
                                    value={item.isSelectedCompany}
                                    title={item.title}
                                    mainCategory={false}
                                    customTextStyle={{ fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM }}
                                    onValueChange={() => this.onToggle(index)}
                                />
                            );
                        }}
                        keyExtractor={item => `${item.id}`}
                    />

                    <Text style={{
                        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM,
                        color: ComponentStyles.COLORS.DARKER_GRAY, fontSize: 18, marginTop: 10, marginBottom: 10
                    }}>{'Update Login Details'}</Text>
                    <InputText style={styles.input} placeholder='Mobile Number' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Old Password' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='New Password' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Confirm Password' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />

                    <ActionButton
                        customTextStyle={{ fontSize: 15 }}
                        customBtnStyle={{ width: 90, height: 40, alignSelf: 'flex-end', marginRight: 0 }}
                        title={'Submit'} onPress={() => this.props.navigation.dispatch(StackActions.pop(1))}
                    />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 50,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM,
        fontSize: 14, paddingBottom: 0, color: ComponentStyles.COLORS.BLUE
    },

    title: {
        fontSize: 15, marginTop: 5, marginBottom: 5,
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD,
        color: ComponentStyles.COLORS.DARKER_GRAY
    }
});