import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity
} from "react-native";
import ComponentStyles from "../../../constants/Component.styles";
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';

export default class CompanyDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            origin: ''
        };
    }

    render() {
        const origin = this.state.origin;
        return (
            <ScrollView style={[ComponentStyles.CONTAINER, { backgroundColor: ComponentStyles.COLORS.SHADE_WHITE }]}>
                <View style={ComponentStyles.CONTENT}>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <Text style={styles.Title}>{'Company Details'}</Text>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('EditCompanyDetails', { origin: origin })}>
                            <IconM name="pencil" size={14} color={ComponentStyles.COLORS.LIGHT_BLUE} style={{ marginRight: 5 }} />
                            <Text style={styles.editInfo}>{'Edit Company Info'}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', padding: 10, }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Company Name'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Display Name'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Contact Number'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Email'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'ZIP Code'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={[styles.text, { height: 50 }]}>{'Address'}</Text>
                            <View style={{ flex: 1 }} />
                        </View>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Dialog Axiata PLC'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Dialog Axiata PLC'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'0712736542'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'service@dialog.lk'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'00700'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={[styles.value, { height: 50 }]}>{'475 Union Place, Colombo'}</Text>
                            <View style={{ flex: 1 }} />
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 16,
        color: ComponentStyles.COLORS.DARKER_GRAY,
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD
    },

    editInfo: {
        fontSize: 11,
        color: ComponentStyles.COLORS.LIGHT_BLUE,
        fontFamily: ComponentStyles.FONT_FAMILY.LIGHT
    },

    text: {
        flex: 1,
        fontSize: 13,
        color: ComponentStyles.COLORS.DARKER_GRAY,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM
    },

    value: {
        flex: 1,
        fontSize: 13,
        color: '#828282',
        fontFamily: ComponentStyles.FONT_FAMILY.REGULAR
    }
});