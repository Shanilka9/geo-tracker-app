import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity
} from "react-native";
import ComponentStyles from "../../../constants/Component.styles";
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Ionicons';

export default class UserDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            origin: ''
        };
    }

    render() {
        const origin = this.state.origin;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={[ComponentStyles.CONTAINER, { backgroundColor: ComponentStyles.COLORS.SHADE_WHITE }]}>
                <View style={ComponentStyles.CONTENT}>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <Text style={styles.Title}>{'Account Details'}</Text>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('EditUserDetails', { origin: origin })}>

                            <IconM name="pencil" size={14} color={ComponentStyles.COLORS.LIGHT_BLUE} style={{ marginRight: 5 }} />
                            <Text style={styles.editInfo}>{'Edit Personal Info'}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', padding: 10, }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Name'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'NIC'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Gender'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Date of Birth'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Contact Number'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Country'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'ZIP Code'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={[styles.text, { height: 50 }]}>{'Primary Address'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={[styles.text, { height: 50 }]}>{'Secondary Address'}</Text>
                            <View style={{ flex: 1 }} />
                        </View>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Kamal Rathnayake'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'54637382v'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Male'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'1984-10-22'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'0712736542'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Sri Lanka'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'80172'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={[styles.value, { height: 50 }]}>{'12/A, Kotte Road, Kalubowila, Colombo'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={[styles.value, { height: 50 }]}>{'325, Udayapura Road, Battaramulla'}</Text>
                            <View style={{ flex: 1 }} />
                        </View>
                    </View>

                    <View style={{ alignItems: 'center', padding: 10, bottom: 5 }}>
                        <TouchableOpacity style={styles.settingsBtn} onPress={()=> this.props.navigation.navigate('Settings')}>
                            <Text style={styles.settingsText}>Settings</Text>
                            <Icon name={'ios-settings-outline'} size={15} color='white' style={{ paddingRight: 5 }} />
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 16,
        color: ComponentStyles.COLORS.DARKER_GRAY,
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD
    },

    editInfo: {
        fontSize: 11,
        color: ComponentStyles.COLORS.LIGHT_BLUE,
        fontFamily: ComponentStyles.FONT_FAMILY.LIGHT
    },

    text: {
        flex: 1,
        fontSize: 13,
        color: ComponentStyles.COLORS.DARKER_GRAY,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM
    },

    value: {
        flex: 1,
        fontSize: 13,
        color: '#828282',
        fontFamily: ComponentStyles.FONT_FAMILY.REGULAR
    },
    settingsBtn: {
        backgroundColor: ComponentStyles.COLORS.BLUE,
        padding: 7, borderRadius: 6, elevation: 6,
        flexDirection: 'row', justifyContent: 'center',
        alignItems: 'center'
    },
    settingsText: {
        paddingLeft: 5,
        paddingRight: 5,
        fontSize: 12,
        color: 'white',
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM
    }
});