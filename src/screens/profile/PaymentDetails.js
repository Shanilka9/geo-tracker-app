import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity
} from "react-native";
import ComponentStyles from "../../../constants/Component.styles";
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';

export default class PaymentDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            origin: ''
        };
    }

    render() {
        const origin = this.state.origin;
        return (
            <ScrollView style={[ComponentStyles.CONTAINER, { backgroundColor: ComponentStyles.COLORS.SHADE_WHITE }]}>
                <View style={ComponentStyles.CONTENT}>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <Text style={styles.Title}>{'Payment Details'}</Text>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('EditPaymentDetails', { origin: origin })}>
                            <IconM name="pencil" size={14} color={ComponentStyles.COLORS.LIGHT_BLUE} style={{ marginRight: 5 }} />
                            <Text style={styles.editInfo}>{'Edit Payment Info'}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', padding: 10, }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Selected Package'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Package User Limit'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Current User Count'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Package Price'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Payment Method'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Started Using Date'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Next Payment Date'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.text}>{'Upgraded Date'}</Text>
                            <View style={{ flex: 1 }} />
                        </View>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'001 - Beta'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'50'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'100'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Rs.100,000.00'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'Visa Card - Annually'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'2019-09-14'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'2020-09-14'}</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.value}>{'2020-01-18'}</Text>
                            <View style={{ flex: 1 }} />
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 16,
        color: ComponentStyles.COLORS.DARKER_GRAY,
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD
    },

    editInfo: {
        fontSize: 11,
        color: ComponentStyles.COLORS.LIGHT_BLUE,
        fontFamily: ComponentStyles.FONT_FAMILY.LIGHT
    },

    text: {
        flex: 1,
        fontSize: 13,
        color: ComponentStyles.COLORS.DARKER_GRAY,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM
    },

    value: {
        flex: 1,
        fontSize: 13,
        color: '#828282',
        fontFamily: ComponentStyles.FONT_FAMILY.REGULAR
    }
});