import React, { Component } from "react";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {
    View,
    Text,
    Image,
    StyleSheet,
} from "react-native";
import ComponentsStyles from "../../../constants/Component.styles";

import UserDetails from "./UserDetails";
import CompanyDetails from "./CompanyDetails";
import PaymentDetails from "./PaymentDetails";

const Tab = createMaterialTopTabNavigator();
const User = 'Admin'
export default class ProfileManager extends Component {
    render() {
        return (
            <View style={ComponentsStyles.CONTAINER}>
                <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 175 }}>
                    <Image style={styles.image} resizeMode='stretch'
                        source={require('../../../assets/images/img-user.png')} />
                    <Text style={styles.text}>Kamal Rathnayake</Text>
                    <Text style={[styles.text, { fontSize: 12 }]}>Dialog Axiata - PLC</Text>
                </View>

                {User === 'User' ?
                    <Tab.Navigator
                        initialRouteName="UserDetails"
                        tabBarOptions={{
                            activeTintColor: ComponentsStyles.COLORS.BLUE,
                            inactiveTintColor: ComponentsStyles.COLORS.DARKER_GRAY,
                            labelStyle: { fontSize: 12, textTransform: 'none', fontFamily: ComponentsStyles.FONT_FAMILY.BOLD, },
                            indicatorStyle: { borderBottomWidth: 3, borderBottomColor: ComponentsStyles.COLORS.BLUE }
                        }}>

                        <Tab.Screen
                            name="UserDetails"
                            component={UserDetails}
                            options={{ tabBarLabel: 'My Account' }} />

                    </Tab.Navigator> : null}

                {User === 'Admin' ?
                    <Tab.Navigator
                        initialRouteName="UserDetails"
                        tabBarOptions={{
                            activeTintColor: ComponentsStyles.COLORS.BLUE,
                            inactiveTintColor: ComponentsStyles.COLORS.DARKER_GRAY,
                            labelStyle: { fontSize: 12, textTransform: 'none', fontFamily: ComponentsStyles.FONT_FAMILY.BOLD, },
                            indicatorStyle: { borderBottomWidth: 3, borderBottomColor: ComponentsStyles.COLORS.BLUE }
                        }}>

                        <Tab.Screen
                            name="UserDetails"
                            component={UserDetails}
                            options={{ tabBarLabel: 'My Account' }} />
                        <Tab.Screen
                            name="CompanyDetails"
                            component={CompanyDetails}
                            options={{ tabBarLabel: 'Company' }} />

                        <Tab.Screen
                            name="PaymentDetails"
                            component={PaymentDetails}
                            options={{ tabBarLabel: 'Payments' }} />

                    </Tab.Navigator> : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: { fontSize: 18, color: '#333333', fontFamily: ComponentsStyles.FONT_FAMILY.MEDIUM },
    image: { height: 90, width: 90, borderRadius: 100, margin: 5 }
});