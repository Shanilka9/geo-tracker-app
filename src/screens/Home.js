/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text, FlatList
} from "react-native";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import { BackHandler } from 'react-native';

import ComponentStyles from "../../constants/Component.styles";
import Header from "../components/Header";
import SwipableItem from "../components/SwipableItem";
import TaskItem from "../components/TaskItem";
import ViewListItem from "../subComponents/ViewListItem";
import Arrays from '../../constants/JSONData'
import OngoingTaskItem from "../subComponents/OngoingTaskItem";

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            itemId: null,
            itemIndex: 0
        }
    }

    componentDidMount() {
        this.props.navigation.addListener('focus', () => {
            this.enableLocation();
        })
        this.enableLocation();
    }

    enableLocation() {
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2>Use Location ?</h2>Please enable GPS to obtain device location for the GEO-TRACKER Application.",
            ok: "OK",
            cancel: "Cancel",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: true, // true => To prevent the location services window from closing when it is clicked outside
            preventBackClick: true, // true => To prevent the location services popup from closing when it is clicked back button
        }).then(function (success) { })
            .catch((error) => { });

        BackHandler.addEventListener('hardwareBackPress', () => {
            LocationServicesDialogBox.forceCloseDialog();
        });

    }

    PressedItem = (itemId) => {
        this.setState({ itemId: itemId })
    }
    render() {
        return (
            <View style={ComponentStyles.CONTAINER}>
                <View style={{
                    position: 'absolute', width: '100%', height: '50%', backgroundColor: ComponentStyles.COLORS.THEME_BLUE,
                    borderBottomLeftRadius: 75, borderBottomRightRadius: 75,
                }} />

                <View style={{ flex: 1, padding: 15, }}>
                    <Header />
                    <View style={{ height: 50, alignSelf: 'center' }}>
                        <FlatList
                            ListEmptyComponent={null}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            data={Arrays.APIResponse.companyList}
                            renderItem={({ item }) => {
                                return (
                                    <ViewListItem title={item.title}
                                        onPress={() => { this.PressedItem(item.id), this.setState({ itemIndex: item.id }) }}
                                        customBtnStyle={this.state.itemIndex == item.id ? { backgroundColor: ComponentStyles.COLORS.BLUE } : null}
                                    />
                                );
                            }}
                            keyExtractor={item => `${item.id}`}
                        />
                    </View>

                    <FlatList
                        ListHeaderComponent={
                            <View>
                                <View style={{ height: 300, alignSelf: 'center' }}>
                                    <FlatList
                                        showsHorizontalScrollIndicator={false}
                                        horizontal={true}
                                        data={Arrays.APIResponse.statusList}
                                        renderItem={({ item }) => {
                                            return (
                                                <SwipableItem title={item.title} color={item.color} />
                                            );
                                        }}
                                        keyExtractor={item => `${item.id}`}
                                    />
                                </View>
                                <View style={{ opacity: 0.98, borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: ComponentStyles.COLORS.WHITE }}>
                                    <Text style={{ fontFamily: ComponentStyles.FONT_FAMILY.BOLD, fontSize: 20, marginTop: 5, marginLeft: 10, padding: 5 }}>Ongoing Tasks</Text>
                                    <FlatList
                                        ListEmptyComponent={null}
                                        showsHorizontalScrollIndicator={false}
                                        horizontal={true}
                                        data={Arrays.APIResponse.ongoingTasksList}
                                        renderItem={({ item }) => {
                                            return (
                                                <OngoingTaskItem
                                                    company={item.company}
                                                    taskName={item.taskName}
                                                    time={item.time}
                                                    backgroundColor={item.color}
                                                    onPress={() => this.props.navigation.navigate('TaskManager')}
                                                />
                                            );
                                        }}
                                        keyExtractor={item => `${item.id}`}
                                    />
                                    <Text style={{ fontFamily: ComponentStyles.FONT_FAMILY.BOLD, fontSize: 20, marginLeft: 10, padding: 5 }}>Today Tasks</Text>
                                </View>

                            </View>
                        }

                        // if data is empty this component will render
                        ListEmptyComponent={<Text>No DETAILS</Text>}

                        // main scrolling flatlist
                        showsVerticalScrollIndicator={false}
                        data={Arrays.APIResponse.taskList}
                        renderItem={({ item }) => {
                            return (
                                <View style={{ opacity: 0.98, backgroundColor: ComponentStyles.COLORS.WHITE }}>
                                    <TaskItem
                                        company={item.company}
                                        taskName={item.taskName}
                                        time={item.time}
                                        date={item.date}
                                        backgroundColor={item.color}
                                        onPress={() => this.props.navigation.navigate('TaskManager')}
                                    />
                                </View>
                            );
                        }}
                        keyExtractor={item => `${item.id}`}
                    />

                </View>
            </View>
        );
    }
}