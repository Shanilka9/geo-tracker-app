/**
 * @author Shanilka
 */
import React, { Component } from "react";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {
    View,
    Text,
    Image,
    StyleSheet,
} from "react-native";
import ComponentsStyles from "../../../constants/Component.styles";

import Details from "./Details";
import Remark from "./Remark";
import Images from "./Images";
import ComponentStyles from "../../../constants/Component.styles";

const Tab = createMaterialTopTabNavigator();

export default class TaskManager extends Component {
    render() {
        return (
            <View style={ComponentsStyles.CONTAINER}>

                <Tab.Navigator
                    initialRouteName="Details"
                    tabBarOptions={{
                        activeTintColor: ComponentsStyles.COLORS.WHITE,
                        inactiveTintColor: '#cccccc',
                        labelStyle: { fontSize: 14, textTransform: 'none', fontFamily: ComponentsStyles.FONT_FAMILY.BOLD, padding: 5 },
                        indicatorStyle: { borderBottomWidth: 3, borderBottomColor: ComponentsStyles.COLORS.WHITE },
                        style: { backgroundColor: ComponentStyles.COLORS.BLUE }
                    }} >

                    <Tab.Screen
                        name="Details"
                        component={Details}
                        options={{ tabBarLabel: 'Task Details' }} />

                    <Tab.Screen
                        name="Remark"
                        component={Remark}
                        options={{ tabBarLabel: 'Member Remarks' }} />

                    {/* <Tab.Screen
                        name="Images"
                        component={Images}
                        options={{ tabBarLabel: 'Member Images' }} /> */}

                </Tab.Navigator>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: { fontSize: 18, color: '#333333', fontFamily: ComponentsStyles.FONT_FAMILY.MEDIUM },
    image: { height: 90, width: 90, borderRadius: 100, margin: 5 }
});