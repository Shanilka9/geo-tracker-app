/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image, ScrollView
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import ComponentStyles from "../../../constants/Component.styles";
import ActionButton from "../../components/ActionButton";
import OutlineButton from "../../subComponents/OutlineButton";

import ProfileImageItem from "../../subComponents/ProfileImageItem";
import StatusItem from "../../subComponents/StatusItem";
import { StackActions } from '@react-navigation/native';

export default class Details extends Component {
    render() {
        return (
            <View style={[ComponentStyles.CONTAINER, { backgroundColor: ComponentStyles.COLORS.SHADE_WHITE }]}>
                <ScrollView showsVerticalScrollIndicator={false} style={ComponentStyles.CONTENT}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                        <Text style={styles.taskId}>{'Task ID : 0002445'}</Text>
                        <StatusItem status={1} />
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Image resizeMode={'contain'} style={{ width: 50, height: 50, borderRadius: 100, marginRight: 5 }} source={require('../../../assets/images/img-user2.png')} />
                        <View style={{ padding: 5 }}>
                            <Text style={[styles.title, { top: 3 }]}>{'Assigned by'}</Text>
                            <Text style={[styles.value, { bottom: 3 }]}>{'Sunanda Punchihewa'}</Text>
                        </View>
                        <View style={styles.onlineDot} />
                    </View>

                    <View>
                        <Text style={[styles.title, { fontSize: 16, padding: 3, marginLeft: 10 }]}>{'Monitored by'}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <ProfileImageItem name={'Sunanda'} image={require('../../../assets/images/img-user2.png')} />
                            <ProfileImageItem name={'Ajith'} image={require('../../../assets/images/img-user.png')} />
                        </View>
                    </View>
                    <View style={ComponentStyles.SEPARATE_LINE} />

                    <View style={{ padding: 10 }}>
                        {/* <View style={{ alignItems: 'center', position: 'absolute', top: 9 }}>
                            <View style={styles.dot} />
                            <View style={styles.line} />
                            <View style={styles.dot} />
                            <View style={styles.line} />
                            <View style={styles.dot} />
                            <View style={styles.line} />
                            <View style={styles.dot} />
                            <View style={styles.line} />
                            <View style={styles.dot} />
                        </View> */}

                        <Text style={styles.title}>{'Task Name'}</Text>
                        <Text style={styles.value}>{'Collect Cheques from Dialog Axiata'}</Text>
                        <View style={ComponentStyles.SEPARATE_LINE} />

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View>
                                <Text style={styles.title}>{'Destination Location'}</Text>
                                <Text style={styles.value}>{'475 Union Place, Colombo'}</Text>
                            </View>
                            <TouchableOpacity>
                                <Icon name='ios-location-sharp' size={25} color={ComponentStyles.COLORS.GREEN} />
                            </TouchableOpacity>
                        </View>
                        <View style={ComponentStyles.SEPARATE_LINE} />

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View>
                                <Text style={styles.title}>{'Starting Date'}</Text>
                                <Text style={styles.value}>{'2020-09-18'}</Text>
                            </View>
                            <View style={{ alignItems: 'flex-end' }}>
                                <Text style={styles.title}>{'Starting Time'}</Text>
                                <Text style={styles.value}>{'10.00 AM'}</Text>
                            </View>
                        </View>
                        <View style={ComponentStyles.SEPARATE_LINE} />

                        <Text style={styles.title}>{'Staying Duration'}</Text>
                        <Text style={styles.value}>{'02h 15min'}</Text>
                        <View style={ComponentStyles.SEPARATE_LINE} />

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View>
                                <Text style={styles.title}>{'Diviation Start by'}</Text>
                                <Text style={styles.value}>{'10 Min'}</Text>
                            </View>
                            <View style={{ alignItems: 'flex-end' }}>
                                <Text style={styles.title}>{'Diviation End by'}</Text>
                                <Text style={styles.value}>{'05 Min'}</Text>
                            </View>
                        </View>
                        <View style={ComponentStyles.SEPARATE_LINE} />

                        <OutlineButton
                            customStyle={{
                                borderRadius: 8, backgroundColor: ComponentStyles.COLORS.SHADE_WHITE,
                                marginBottom: 15, height: 45, elevation: 5
                            }}
                            customTextStyle={{ fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, fontSize: 15, color: ComponentStyles.COLORS.BLUE }}
                            name={'Request Replacement'}
                            onPress={() => this.props.navigation.dispatch(StackActions.pop(1))}
                        />
                    </View>

                </ScrollView>

                <ActionButton title={'Start Task'} onPress={() => this.props.navigation.dispatch(StackActions.pop(1))} />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    taskId: { fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: ComponentStyles.COLORS.BLUE },
    title: { fontFamily: ComponentStyles.FONT_FAMILY.REGULAR, color: ComponentStyles.COLORS.DARKER_GRAY, fontSize: 13 },
    value: { fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: ComponentStyles.COLORS.GRAY, fontSize: 15 },
    // dot: { width: 12, height: 12, backgroundColor: ComponentStyles.COLORS.BLUE, borderRadius: 100, elevation: 4 },
    // line: { width: 0.5, backgroundColor: ComponentStyles.COLORS.BLUE, height: 66 },
    onlineDot: {
        position: 'absolute', width: 12, height: 12, borderRadius: 100, bottom: 8, left: 38,
        backgroundColor: '#32CD32', borderWidth: 0.8, borderColor: 'white'
    }
});