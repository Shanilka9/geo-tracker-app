/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Text
} from "react-native";
import { StackActions } from '@react-navigation/native';
import ComponentStyles from "../../../constants/Component.styles"
import RegistrationManager from "../../../singleton/RegistrationManager"
import ActionButton from "../../components/ActionButton"
import InputText from '../../components/InputText'
import MultiSelect from "../../components/MultiSelect"
import ImagePicker from 'react-native-image-picker';

import Moment from 'moment'
import DatePicker from 'react-native-date-picker'
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Octicons'
import RadioForm from 'react-native-radio-form';
import SubHeader from "../../subComponents/SubHeader";

const countryArray = [
    { "id": 0, "name": "Srilanka" }, { "id": 3, "name": "India" },
    { "id": 2, "name": "China" }, { "id": 1, "name": "USA" },
];

const genderArray = [{ label: 'Male', value: 'M' }, { label: 'Female', value: 'F' }];

export default class EditUserDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imagePath: null,
            selected_country: '',
            dob: new Date(),
            isModalVisible: false,
            newDate: '',
            imagePath: null,
            imageType: null,
            imageName: null,
            is_photo_taken: false
        }
    }

    componentDidMount() {
        RegistrationManager.clearAll(); //clear all details
    }

    showDatePicker = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible,
            newDate: Moment(this.state.dob).format('YYYY-MM-DD').toString()
        });
    };

    onSelectedCountry = (selectedCountry) => {
        if (selectedCountry.length > 0) {
            this.setState({ countryState: true })
        } else {
            this.setState({ countryState: false })
        }
        this.setState({ selectedCountry })
        this.setState({ selected_country: selectedCountry[0] })
        RegistrationManager._selectedCountry = selectedCountry; //save country data in singleton
    }

    selectPhotoTapped() {
        const options = {
            mediaType: 'photo',
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            skipBackup: true,
            privateDirectory: true,
            cameraRoll: false,
            noData: true,
            cameraType: 'front'
            // storageOptions: {
            //     skipBackup: true,
            // },
        };

        ImagePicker.showImagePicker(options, response => {
            // console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let path = (Platform.OS == 'android') ? response.uri : response.uri.replace('file://', '');
                this.setState({
                    imagePath: path,
                    imageType: response.type,
                    imageName: response.fileName
                });

                //update value in singleton
                // RegistrationManager._form5ImgPath = path;
                // RegistrationManager._form5ImgType = response.type;
                // RegistrationManager._form5ImgName = response.fileName;
                // RegistrationManager._form5IsImageUploaded = false;

                if (this.state.imagePath) {
                    this.setState({ is_photo_taken: true });
                }
            }
        });

    }

    _onSelect = (item) => {
        console.log(item.label);
    };

    /**
     * return the form data object of the image
     */
    getImageFormData() {
        let formdata = new FormData();
        let imageName = this.state.imageName;
        if (!this.state.imageName && this.state.imagePath) {
            var split = this.state.imagePath.split('/');
            if (split && Array.isArray(split) && split.length > 0) {
                imageName = split[split.length - 1];
            }
        }

        formdata.append('file', {
            uri: this.state.imagePath,
            type: this.state.imageType,
            name: imageName
        });

        return formdata;
    }

    renderDatePickerModal() {
        return (
            <Modal isVisible={this.state.isModalVisible}>
                <View style={styles.alertBox}>
                    <DatePicker
                        mode={'date'}
                        style={{ bottom: 20 }}
                        date={this.state.dob}
                        maximumDate={new Date()}
                        onDateChange={date => {
                            if (!date || !Moment(date) || !Moment(date).isValid()) {
                                this.setState({ dob: null });
                                return;
                            }
                            this.setState({ dob: date });
                        }}
                    />

                    <ActionButton customBtnStyle={{
                        position: 'absolute', alignSelf: 'center', paddingLeft: 80, paddingRight: 80,
                        marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0,
                    }} title={'Done'} onPress={this.showDatePicker} />
                </View>
            </Modal>
        );

    }

    render() {
        const { origin } = this.props.route.params;
        return (
            <View style={ComponentStyles.CONTAINER}>
                {/* {console.log("ORIGIN :", JSON.stringify(origin))} */}
                {origin == 'signup' ?
                    <SubHeader title={'Enter Personal Details'} navigation={() => this.props.navigation.goBack()} /> :
                    <SubHeader title={'Edit Personal Details'} navigation={() => this.props.navigation.goBack()} />}

                {this.renderDatePickerModal()}

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={ComponentStyles.CONTENT}>

                    <View style={{ flexDirection: 'column', alignSelf: "center" }}>
                        <View style={styles.imgCircle}>

                            <View style={styles.imgContainer}>
                                {this.state.imagePath === null ? (<Image source={require('../../../assets/images/ic_selfie_indicator.png')} resizeMode='stretch' style={{ width: '100%', height: '100%', borderRadius: 300 }} />) : (<Image source={{ uri: this.state.imagePath }} resizeMode='cover' style={{ width: '100%', height: '100%', flex: 1, borderRadius: 300 }} />)}
                            </View>

                            <TouchableOpacity style={styles.addBtn} onPress={this.selectPhotoTapped.bind(this)}>
                                <Icon name='plus' size={30} color='white' />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <InputText style={styles.input} placeholder='First Name' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Last Name' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='NIC' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />

                    <View style={styles.genContainer}>
                        <Text style={styles.genText}>{'Gender'}</Text>
                        <RadioForm
                            dataSource={genderArray}
                            itemShowKey="label"
                            itemRealKey="value"
                            circleSize={22}
                            initial={0}
                            formHorizontal={true}
                            labelHorizontal={true}
                            onPress={(item) => this._onSelect(item)}
                        />
                    </View>
                    <View style={{ padding: 4 }} />
                    <TouchableOpacity onPress={() => this.showDatePicker()}>
                        <InputText
                            editable={false}
                            stateValue={this.state.newDate.toString()}
                            style={styles.input}
                            placeholder='Date of Birth'
                            placeholderColor='black' />
                    </TouchableOpacity>

                    <InputText style={styles.input} placeholder='Contact Number' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Primary Address' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Secondary Address' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Zip Code' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />

                    <MultiSelect placeholderText={'Select Country'}
                        dataArray={countryArray} uniqueKey='id' displayKey='name'
                        selectText={'Country'} confirmText={'Confirm'}
                        onSelectedItemsChange={this.onSelectedCountry}
                        selectedItems={this.state.selectedCountry}
                    />
                    <View style={{ padding: 10 }} />


                </ScrollView>

                {origin == 'signup' ?
                    <ActionButton customBtnStyle={{ marginBottom: 30 }} title={'Next'} onPress={() => this.props.navigation.navigate('EditCompanyDetails', { origin: origin })} /> :
                    <ActionButton customBtnStyle={{ marginBottom: 30 }} title={'Update'} onPress={() => this.props.navigation.dispatch(StackActions.pop(1))} />}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 50,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM,
        fontSize: 14, paddingBottom: 0, color: ComponentStyles.COLORS.BLUE
    },

    addBtn: {
        alignSelf: 'center', bottom: 5, right: 10, borderRadius: 100, width: 50, height: 50,
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: ComponentStyles.COLORS.BLUE, position: 'absolute',
    },

    genContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 6,
        marginTop: 3,
        paddingLeft: 8,
        borderWidth: 1,
        borderColor: '#C4C4C4'
    },

    genText: { fontSize: 15, paddingRight: 20, fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: '#4A4958' },

    alertBox: {
        flex: 0.5,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: ComponentStyles.COLORS.WHITE,
        padding: 15,
        borderRadius: 10,
    },

    imgCircle: {
        width: 200,
        height: 200,
        margin: 15,
        borderWidth: 0.1,
        borderRadius: 200,
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        ...Platform.select({
            ios: {
                shadowColor: '#A5A5A5',
                shadowOffset: { height: 4, width: 0 },
                shadowOpacity: 0.7,
                shadowRadius: 5
            },
            android: {
                shadowOpacity: 0.2,
                elevation: 10,
                shadowColor: '#A5A5A5',
            }
        }),
    },

    imgContainer: {
        width: 200, height: 200, borderRadius: 200, justifyContent: 'center',
        alignItems: 'center', borderColor: ComponentStyles.COLORS.BLUE, borderWidth: 3
    }
});

// import { WebView } from 'react-native-webview';
{/* <WebView source={{ uri: 'https://reactnative.dev/' }} /> */ }

// import DateTimePicker from "../../components/DateTimePicker"
// 
{/* <DateTimePicker
    placeholder={'Date of Birth'}
    // minimumDate={new Date()}
    // selectedDate={this.state.dob}
    onDateChanged={(date) => {
        if (!date || !Moment(date) || !Moment(date).isValid()) {
            this.setState({ dob: null });
            return;
        }
        this.setState({ dob: date });
        // console.log("A date has been picked: ", Moment(date).isValid());
    }}
/> */}
