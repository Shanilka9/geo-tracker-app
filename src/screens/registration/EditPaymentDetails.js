/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    StyleSheet,
    ScrollView,
} from "react-native";
import { StackActions } from '@react-navigation/native';
import ComponentStyles from "../../../constants/Component.styles"
import RegistrationManager from "../../../singleton/RegistrationManager"
import ActionButton from "../../components/ActionButton"
import InputText from '../../components/InputText'
import SubHeader from "../../subComponents/SubHeader";

export default class EditPaymentDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        const { origin } = this.props.route.params;
        return (
            <View style={ComponentStyles.CONTAINER}>
                {origin == 'signup' ?
                    <SubHeader title={'Enter Payment Details'} navigation={() => this.props.navigation.goBack()} /> :
                    <SubHeader title={'Edit Payment Details'} navigation={() => this.props.navigation.goBack()} />}
                <ScrollView showsVerticalScrollIndicator={false} style={ComponentStyles.CONTENT}>
                    <View style={{ padding: 5 }} />
                    <InputText style={styles.input} placeholder='Selected Package Name' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Package User Limit' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Current User Count' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Package Price' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                    <InputText style={styles.input} placeholder='Payment Method' placeholderColor={ComponentStyles.COLORS.DARKER_GRAY} />
                </ScrollView>

                {origin == 'signup' ?
                    <ActionButton customBtnStyle={{ marginBottom: 30 }} title={'Submit'} onPress={() => this.props.navigation.navigate('DrawerHome', { origin: origin })} /> :
                    <ActionButton customBtnStyle={{ marginBottom: 30 }} title={'Update'} onPress={() => this.props.navigation.dispatch(StackActions.pop(1))}/>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 50,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM,
        fontSize: 14, paddingBottom: 0, color: ComponentStyles.COLORS.BLUE
    }

});
