/**
 * @author Shanilka
 */
import React, { Component } from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BackHandler } from 'react-native';
import { StackActions } from '@react-navigation/native';

import Icon from 'react-native-vector-icons/Ionicons';
import Home from './Home'
import Location from './Location'
import People from './People'
import Settings from './Settings'
import ProfileManager from './profile/ProfileManager'
import ComponentStyles from "../../constants/Component.styles";

const Tab = createBottomTabNavigator();

export default class HomeManager extends Component {
    constructor(props) {
        super(props)
        // this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    // UNSAFE_componentWillMount() {
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    // }

    // componentWillUnmount() {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    // }

    // handleBackButtonClick() {
    //     this.props.navigation.dispatch(StackActions.popToTop());
    // }

    render() {
        return (
            <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: ComponentStyles.COLORS.BLUE,
                    inactiveTintColor: '#747474',
                    animationEnabled: true,
                    showLabel: false,
                }}
            >
                <Tab.Screen name="Home" component={Home}
                    options={{
                        tabBarIcon: ({ color, size, focused }) => {
                            return (
                                focused
                                    ? <Icon name="ios-home" size={27} color={ComponentStyles.COLORS.BLUE} />
                                    : <Icon name="ios-home-outline" size={27} color="#747474" />
                            )
                        },
                    }}
                />

                <Tab.Screen name="Location" component={Location}
                    options={{
                        tabBarIcon: ({ color, size, focused }) => {
                            return (
                                focused
                                    ? <Icon name="ios-location" size={27} color={ComponentStyles.COLORS.BLUE} />
                                    : <Icon name="ios-location-outline" size={27} color="#747474" />
                            )
                        },
                    }}
                />

                <Tab.Screen name="People" component={People}
                    options={{
                        tabBarIcon: ({ color, size, focused }) => {
                            return (
                                focused
                                    ? <Icon name="ios-calendar" size={27} color={ComponentStyles.COLORS.BLUE} />
                                    : <Icon name="ios-calendar-outline" size={27} color="#747474" />
                            )
                        },
                    }}
                />

                {/* <Tab.Screen name="Settings" component={Settings}
                    options={{
                        tabBarIcon: ({ color, size, focused }) => {
                            return (
                                focused
                                    ? <Icon name="ios-settings" size={27} color={ComponentStyles.COLORS.BLUE} />
                                    : <Icon name="ios-settings-outline" size={27} color="#747474" />
                            )
                        },
                    }}
                /> */}

                <Tab.Screen name="Profile" component={ProfileManager}
                    options={{
                        tabBarIcon: ({ color, size, focused }) => {
                            return (
                                focused
                                    ? <Icon name="ios-person-circle-sharp" size={27} color={ComponentStyles.COLORS.BLUE} />
                                    : <Icon name="ios-person-circle-outline" size={27} color="#747474" />
                            )
                        },
                    }}
                />

            </Tab.Navigator>

        );
    }
}
