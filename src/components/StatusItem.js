/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";
import IconM from 'react-native-vector-icons/MaterialIcons';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import ComponentStyles from "../../constants/Component.styles";

export default class StatusItem extends Component {
    render() {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {this.props.icon === 'pending-actions' ?
                    <IconM
                        name={this.props.icon}
                        style={styles.icon}
                        size={25} color={ComponentStyles.COLORS.WHITE} /> : null}

                {this.props.icon === 'clock-outline' || this.props.icon === 'spellcheck' || this.props.icon === 'timer-sand' ?
                    <IconMC
                        name={this.props.icon}
                        style={styles.icon}
                        size={25} color={ComponentStyles.COLORS.WHITE} /> : null}

                {this.props.icon !== 'pending-actions' && this.props.icon !== 'clock-outline'
                    && this.props.icon !== 'spellcheck' && this.props.icon !== 'timer-sand' ?
                    <Icon
                        name={this.props.icon}
                        style={styles.icon}
                        size={25} color={ComponentStyles.COLORS.WHITE} /> : null}

                <View style={{ padding: 8 }}>
                    <Text style={styles.name}>{this.props.status}</Text>
                    <Text style={styles.count}>{this.props.count}</Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    name: {
        color: ComponentStyles.COLORS.WHITE,
        fontSize: 13,
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD
    },
    count: {
        color: ComponentStyles.COLORS.WHITE,
        fontSize: 14,
        fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM
    },
    icon: {
        padding: 8, borderRadius: 5,
        backgroundColor: '#969696',
        opacity: 0.7
    }
});