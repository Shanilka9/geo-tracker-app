/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
} from "react-native";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import ComponentStyles from "../../constants/Component.styles";

export default class MultiSelect extends Component {
    render() {
        return (
            <View>
                <SectionedMultiSelect
                    searchPlaceholderText={this.props.placeholderText}
                    colors={{ cancel: ComponentStyles.COLORS.BLUE, primary: ComponentStyles.COLORS.BLUE }}
                    itemFontFamily={{ fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM }}
                    confirmFontFamily={{ fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM }}
                    searchTextFontFamily={{ fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, fontSize: 16, }}
                    styles={{
                        container: {
                            marginTop: getStatusBarHeight(true) + 10
                        },
                        selectToggleText: this.props.buttonText
                            ? this.props.buttonText
                            : { color: ComponentStyles.COLORS.BLUE, fontSize: 15, fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM },
                        selectToggle: this.props.button
                            ? this.props.button
                            : {
                                borderColor: '#C4C4C4',
                                borderWidth: 1,
                                backgroundColor: ComponentStyles.COLORS.WHITE,
                                borderRadius: 4,
                                height: 50,
                                padding: 10,
                                marginTop: 10,
                            }
                    }}
                    disabled={this.props.disabled == undefined ? false : this.props.disabled}
                    showCancelButton={true}
                    container={{ backgroundColor: '#e0e0e0' }}
                    button={{ color: '#f2f2f2' }}
                    items={this.props.dataArray}
                    displayKey={this.props.displayKey}
                    single={true}
                    uniqueKey={this.props.uniqueKey}
                    // subKey='children'
                    selectText={this.props.selectText}
                    // showDropDowns={true}
                    // readOnlyHeadings={true}
                    onSelectedItemsChange={this.props.onSelectedItemsChange}
                    selectedItems={this.props.selectedItems}
                    confirmText=""
                    hideConfirm={false}
                    modalWithTouchable={true}
                />
            </View>
        );
    }
}