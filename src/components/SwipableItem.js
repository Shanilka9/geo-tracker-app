/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";
import StatusItem from "./StatusItem";

export default class SwipableItem extends Component {
    render() {
        return (
            <View style={[styles.container, { backgroundColor: this.props.color }]}>
                <Text style={{
                    fontFamily: ComponentStyles.FONT_FAMILY.BOLD,
                    fontSize: 20,
                    color: ComponentStyles.COLORS.WHITE
                }}>{this.props.title}</Text>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <StatusItem icon={'pending-actions'} status={'Pending'} count={'2/10'} />
                        <StatusItem icon={'spellcheck'} status={'Arrived'} count={'1/10'} />
                        <StatusItem icon={'ios-checkmark-circle-outline'} status={'Finished'} count={'2/10'} />
                    </View>
                    <View style={{ padding: 20 }} />
                    <View style={{ flex: 1 }}>
                        <StatusItem icon={'clock-outline'} status={'Started'} count={'3/10'} />
                        <StatusItem icon={'checkmark-done'} status={'Completed'} count={'6/10'} />
                        <StatusItem icon={'timer-sand'} status={'Stayed Duration'} count={'2'} />
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        padding: 20, margin: 10,
        borderRadius: 10, elevation:10,
        justifyContent: 'space-evenly'
    }
});