/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Image, TouchableOpacity
} from "react-native";
import TrackSwitch from "../subComponents/TrackSwitch";
// 
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconM from 'react-native-vector-icons/MaterialIcons';
import ComponentStyles from "../../constants/Component.styles";

export default class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            toggle: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/images/img-user.png')} resizeMode={'contain'} style={{ width: 45, height: 45, borderRadius: 100 }} />
                <TrackSwitch onPress={() => this.setState({ toggle: !this.state.toggle })} track={this.state.toggle} />

                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity>
                        <IconM name={'chat'} color={ComponentStyles.COLORS.WHITE} size={25} />
                    </TouchableOpacity>
                    <View style={{ padding: 10 }} />
                    <TouchableOpacity>
                        <Icon name={'bell-ring'} color={ComponentStyles.COLORS.WHITE} size={25} />
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    }
});