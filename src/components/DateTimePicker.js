import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/Ionicons';
import Moment from 'moment';

export default class DateTimePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDatePickerVisible: false,
            date: (this.props.placeholder) ? this.props.placeholder : ''
        };
    }

    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true });
    };

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false });
    };

    handleConfirm = async (date, stopTriggerOnChange) => {
        this.hideDatePicker();
        try {
            this.setState({ date: this.getDateString(date) }, () => {
                if (this.props.onDateChanged && !stopTriggerOnChange)
                    this.props.onDateChanged(this.state.date);
            });
        } catch (error) {
            console.log('date time picker error: ', error);
        }
    };

    getDateString = (date) => {
        let dateStr = '';
        try {
            let mode = 'date';
            if (this.props.mode) mode = this.props.mode;

            if (mode == 'date') dateStr = Moment(date).format('YYYY-MM-DD');
            else if (mode == 'datetime') dateStr = Moment(date).format('YYYY-MM-DD hh:mm a');
            else if (mode == 'time') dateStr = Moment(date).format('hh:mm a');
        } catch (error) {
            console.log('date string create error: ', error);
        }

        return dateStr;
    }

    render() {
        return (
            <View>
                <TouchableOpacity style={{
                    flex: 1, flexDirection: 'row', padding: 10, borderColor: '#C4C4C4', borderWidth: 1,
                    backgroundColor: 'white', borderRadius: 4,
                }}
                    onPress={this.showDatePicker}>
                    <Text style={{
                        flex: 1, color: '#1D3D5A', fontSize: 15, marginLeft: 2, fontFamily: 'Poppins-Regular'
                    }}>{(this.props.selectedDate && Moment(this.props.selectedDate) && Moment(this.props.selectedDate).isValid())
                        ? this.getDateString(this.props.selectedDate) : this.getDateString(Moment())}</Text>
                    <Icon name="ios-calendar" size={25} color="#000" />
                </TouchableOpacity>
                <DateTimePickerModal
                    date={(this.props.selectedDate && Moment(this.props.selectedDate) && Moment(this.props.selectedDate).isValid())
                        ? Moment(this.props.selectedDate).toDate() : new Date()}
                    isVisible={this.state.isDatePickerVisible}
                    minimumDate={this.props.minimumDate ?? null}
                    mode={this.props.mode ?? 'date'}
                    onConfirm={this.handleConfirm}
                    onCancel={this.hideDatePicker}
                />
            </View >
        );
    }

}