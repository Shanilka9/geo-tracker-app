/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet, TouchableOpacity
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";

export default class ViewListItem extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.container, this.props.customBtnStyle]} onPress={() => this.props.onPress()} >
                <Text style={{
                    color: 'white', fontSize: 12,
                    fontFamily: ComponentStyles.FONT_FAMILY.BOLD
                }}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 5,
        elevation: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'gray',
        padding: 20, borderRadius: 50
    }
});