/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";

export default class StatusItem extends Component {
    render() {
        return (
            <View>
                {this.props.status == 1 ?
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.circle, { backgroundColor: ComponentStyles.COLORS.ORANGE }]} />
                        <Text style={styles.text}>Pending</Text>
                    </View> : null}

                {this.props.status == 2 ?
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.circle, { backgroundColor: ComponentStyles.COLORS.GREEN }]} />
                        <Text style={styles.text}>Completed</Text>
                    </View> : null}

                {this.props.status == 3 ?
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.circle, { backgroundColor: ComponentStyles.COLORS.LIGHT_BLUE }]} />
                        <Text style={styles.text}>In Progress</Text>
                    </View> : null}

                {this.props.status == 4 ?
                    <View style={{ flexDirection: 'row' }}>
                        <View style={[styles.circle, { backgroundColor: ComponentStyles.COLORS.RED }]} />
                        <Text style={styles.text}>Cancelled</Text>
                    </View> : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    circle: { width: 12, height: 12, margin: 5, borderRadius: 100 },
    text: { fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: ComponentStyles.COLORS.DARKER_GRAY }
});