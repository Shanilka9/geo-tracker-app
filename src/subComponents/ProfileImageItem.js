/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";

export default class ProfileImageItem extends Component {
    render() {
        return (
            <View style={{ width:75, justifyContent: 'center', alignItems: 'center' }}>
                <Image style={styles.image} resizeMode={'contain'} source={this.props.image} />
                <Text style={styles.value}>{this.props.name}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    value: { fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: ComponentStyles.COLORS.GRAY, fontSize: 12 },
    image: { width: 50, height: 50, borderRadius: 100, }
});