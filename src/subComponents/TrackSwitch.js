/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";

export default class TrackSwitch extends Component {
  render() {
    return (
      <View>
        {this.props.track ?
          <TouchableOpacity style={styles.container} onPress={() => this.props.onPress()}>
            <Text style={[styles.text, { paddingLeft: 8 }]}>Tracking ON</Text>
            <View style={styles.circle} />
          </TouchableOpacity>
          :
          <TouchableOpacity style={[styles.container, { backgroundColor: 'gray' }]} onPress={() => this.props.onPress()}>
            <View style={[styles.circle, { marginLeft: 0, marginRight: 5 }]} />
            <Text style={[styles.text, { paddingRight: 5 }]}>Tracking OFF</Text>
          </TouchableOpacity>}

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ComponentStyles.COLORS.BLUE,
    width: 140, height: 40,
    // padding: 8,
    borderRadius: 50
  },
  text: {
    fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM,
    fontSize: 14, color: 'white'
  },

  circle: {
    padding: 15, marginLeft: 5,
    borderRadius: 50, backgroundColor: 'white'
  }
});