/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";
import { Switch } from 'react-native-paper';
import ComponentStyles from "../../constants/Component.styles";

export default class ToggleSwitch extends Component {
    
    render() {
        return (
            <View>
                {this.props.mainCategory ?
                    <View style={styles.container}>
                        <Text style={[styles.title, this.props.customTextStyle]}>{this.props.title}</Text>
                        <Switch
                            value={this.props.value}
                            color={ComponentStyles.COLORS.BLUE}
                            onValueChange={() => this.props.onValueChange()}
                        />
                    </View>
                    :
                    <View style={[styles.container, { justifyContent: 'flex-start' }]}>
                        <Switch
                            value={this.props.value}
                            color={ComponentStyles.COLORS.BLUE}
                            onValueChange={() => this.props.onValueChange()}
                        />
                        <Text style={[styles.title, this.props.customTextStyle]}>{this.props.title}</Text>

                    </View>
                }
            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 15, marginTop: 5, marginBottom: 5,
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD,
        color: ComponentStyles.COLORS.DARKER_GRAY
    }
});