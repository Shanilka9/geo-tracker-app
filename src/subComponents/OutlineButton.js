/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";
import comStyles from '../../constants/Component.styles'

export default class OutlineButton extends Component {

    render() {
        return (
            <TouchableOpacity style={[styles.button, this.props.customStyle]} onPress={this.props.onPress}>
                <Text style={[styles.input, this.props.customTextStyle]}>{this.props.name}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({

    button: {
        borderColor: comStyles.COLORS.BLUE,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1.5,
        borderRadius: 5,
        height: 56,
    },

});