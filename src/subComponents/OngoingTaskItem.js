/**
 * @author Shanilka
 */
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet, TouchableOpacity
} from "react-native";
import ComponentStyles from "../../constants/Component.styles";
import ActionButton from '../components/ActionButton'
export default class OngoingTaskItem extends Component {
    render() {
        var company = this.props.company;
        var letter = company.charAt(0);
        return (
            <View style={styles.container}>
                <Text style={[styles.letter, { backgroundColor: this.props.backgroundColor }]}>{letter}</Text>
                <View style={{ flex: 1 }}>
                    <Text style={styles.company}>{this.props.company}</Text>
                    <Text style={styles.taskName}>{this.props.taskName}</Text>
                </View>
                <ActionButton
                    title={'View More'}
                    onPress={()=> this.props.onPress()}
                    customBtnStyle={{ height: 40, padding: 10, marginLeft: 20, marginRight: 0, marginTop: 0, marginBottom: 0 }}
                    customTextStyle={{ fontSize: 12 }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15, margin: 10,
        backgroundColor: 'white',
        elevation: 6, borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    letter: {
        width: 40, height: 40,
        textAlign: 'center', textAlignVertical: 'center',
        fontFamily: ComponentStyles.FONT_FAMILY.BOLD,
        fontSize: 25, color: ComponentStyles.COLORS.WHITE,
        borderRadius: 100, marginRight: 10
    },

    company: { flex: 1, fontFamily: ComponentStyles.FONT_FAMILY.BOLD, fontSize: 13 },
    taskName: { flex: 1, fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: '#737373', fontSize: 13 },
    time: { flex: 1, fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, color: ComponentStyles.COLORS.BLUE, fontSize: 12 },
    date: { flex: 1, fontFamily: ComponentStyles.FONT_FAMILY.MEDIUM, fontSize: 10, color: '#737373', },
});