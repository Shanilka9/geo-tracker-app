/**
 * @author Shanilka
 */
class RegistrationManager {
    static instance = null;

    //form 1
    _selectedCountry = null;
    _gender = null;

    static getInstance() {
        if (!RegistrationManager.instance) {
            RegistrationManager.instance = new RegistrationManager();
        }

        return RegistrationManager.instance;
    }

    /**
     * clear 1st registration screen data
     */
    clearRegistrationData1() {
        this._selectedCountry = null;
    }

    /**
     * clear all data in the singleton
     */
    clearAll() {
        this.clearRegistrationData1();
    }
}

export default new RegistrationManager();