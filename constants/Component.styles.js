/**
 * @author Shanilka
 */
import { Dimensions } from "react-native";
import { getStatusBarHeight } from 'react-native-status-bar-height';
const { width, height } = Dimensions.get('window');

const BACKGROUND_COLOR = "#FFFFFF";

export default ({
    WIDTH: width,
    HEIGHT: height,
    COLORS: {
        THEME_BLUE: '#1F2EDB',
        WHITE: '#FFFFFF',
        SHADE_WHITE: '#F2F2F2',
        BLACK: '#000000',
        GREEN: '#088F43',
        RED: '#EE1C1E',
        BLUE: '#2874ed',
        LIGHT_BLUE: '#2F80ED',
        GRAY: '#333333',
        DARKER_GRAY: '#4D5A61',
        ORANGE: '#ff8400'
    },
    FONT_FAMILY: {
        BOLD: 'Poppins-Bold',
        SEMI_BOLD: 'Poppins-Semibold',
        MEDIUM: 'Poppins-Medium',
        REGULAR: 'Poppins-Regular',
        LIGHT: 'Poppins-Light'
    },

    CONTAINER: {
        backgroundColor: BACKGROUND_COLOR,
        paddingTop: getStatusBarHeight(true),
        flex: 1
    },
    CONTENT: {
        margin: 25,
        marginTop: 0,
        marginBottom: 0,
        paddingTop: 13,
        flex: 1,
    },
    SEPARATE_LINE: {
        backgroundColor: '#D0CFCF',
        width: '100%',
        height: 0.9,
        marginTop: 15,
        marginBottom: 15,
    },
    TOP_RIGHT_MENU: {
        position: 'absolute',
        right: 5,
        top: 5
    }
})