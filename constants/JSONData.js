/**
 * @author Shanilka
 */
export default {
    "Agenda": {
        '2020-09-27': [
            {
                company: 'Dialog Axiata PLC',
                task: 'Collect Cheques',
                time: '09.30am - 11.30am',
                address: '475, Union Place, Colombo',
            }
        ],
        '2020-09-28': [
            {
                company: 'Srilanka Telecom PLC',
                task: 'Collect new user details',
                time: '10.30am - 12.05pm',
                address: '475, Union Place, Colombo',
            },
            {
                company: 'Mobitel PLC',
                task: 'Request SRS of new development',
                time: '12.30am - 01.00pm',
                address: 'Ward Place, Colombo 11',
            },
            {
                company: 'Dialog Axiata PLC',
                task: 'Collect Cheques',
                time: '09.30am - 11.30am',
                address: '475, Union Place, Colombo',
            }
        ],
        '2020-09-29': [
            {
                company: 'Srilanka Telecom PLC',
                task: 'Collect new user details',
                time: '10.30am - 12.05pm',
                address: '475, Union Place, Colombo',
            },
            {
                company: 'Mobitel PLC',
                task: 'Request SRS of new development',
                time: '12.30am - 01.00pm',
                address: 'Ward Place, Colombo 11',
            }
        ], '2020-09-30': [
            {
                company: 'Srilanka Telecom PLC',
                task: 'Collect new user details',
                time: '10.30am - 12.05pm',
                address: '475, Union Place, Colombo',
            }
        ]
    },
    "APIResponse": {
        "companyList": [
            { id: 0, title: 'Mine' },
            { id: 1, title: 'Srilanka Telecom PLC' },
            { id: 2, title: 'Dialog Axiata' }
        ],
        "statusList": [
            { id: 0, title: 'Today - All', color: '#2874ed' },
            { id: 1, title: 'This Week', color: '#00b542' },
            { id: 2, title: 'This Month', color: '#8f45ff' }
        ],
        "ongoingTasksList": [
            {
                id: 0,
                company: 'Dialog Axiata PLC',
                taskName: 'Collect Cheques',
                color: '#2874ed'
            },
            {
                id: 1,
                company: 'Sri Lanka Telecom - PLC',
                taskName: 'Collect Cheques',
                color: '#e06500'
            },
        ],
        "taskList": [
            {
                id: 0,
                company: 'Dialog Axiata PLC',
                taskName: 'Collect Cheques',
                time: '09.00am',
                date: '17/09/2020',
                color: '#2874ed'
            },
            {
                id: 1,
                company: 'Mobitel PLC',
                taskName: 'Collect Cheques',
                time: '10.00am',
                date: '17/09/2020',
                color: '#8f45ff'
            },
            {
                id: 2,
                company: 'Sri Lanka Telecom - PLC',
                taskName: 'Collect Cheques',
                time: '12.00am',
                date: '17/09/2020',
                color: '#e06500'
            },
            {
                id: 3,
                company: 'Zincat Technologies',
                taskName: 'Collect Cheques',
                time: '01.00pm',
                date: '17/09/2020',
                color: '#088F43'
            }
        ],
    },

    "RegisteredCompanyList": [
        { id: 0, title: 'Dialog Axiata' },
        { id: 1, title: 'Srilanka Telecom PLC' },
        { id: 2, title: 'Mobitel PLC' },
    ]
    // "APIResponse2": {
    //     "companyList": [
    //         {
    //             id: 0,
    //             title: 'Mine',
    //             "statusList": [
    //                 { id: 0, title: 'Today - All', color: '#2874ed' },
    //                 { id: 1, title: 'This Week', color: '#00b542' },
    //                 { id: 2, title: 'This Month', color: '#8f45ff' }
    //             ],
    //             "ongoingTasksList": [
    //                 {
    //                     id: 0,
    //                     company: 'Dialog Axiata PLC',
    //                     taskName: 'Collect Cheques',
    //                     color: '#2874ed'
    //                 },
    //                 {
    //                     id: 1,
    //                     company: 'Sri Lanka Telecom - PLC',
    //                     taskName: 'Collect Cheques',
    //                     color: '#e06500'
    //                 },
    //             ],
    //             "taskList": [
    //                 {
    //                     id: 0,
    //                     company: 'Dialog Axiata PLC',
    //                     taskName: 'Collect Cheques',
    //                     time: '09.00am',
    //                     date: '17/09/2020',
    //                     color: '#2874ed'
    //                 },
    //                 {
    //                     id: 1,
    //                     company: 'Mobitel PLC',
    //                     taskName: 'Collect Cheques',
    //                     time: '10.00am',
    //                     date: '17/09/2020',
    //                     color: '#8f45ff'
    //                 },
    //                 {
    //                     id: 2,
    //                     company: 'Sri Lanka Telecom - PLC',
    //                     taskName: 'Collect Cheques',
    //                     time: '12.00am',
    //                     date: '17/09/2020',
    //                     color: '#e06500'
    //                 },
    //                 {
    //                     id: 3,
    //                     company: 'Zincat Technologies',
    //                     taskName: 'Collect Cheques',
    //                     time: '01.00pm',
    //                     date: '17/09/2020',
    //                     color: '#088F43'
    //                 }
    //             ]
    //         },
    //         {
    //             id: 1,
    //             title: 'Srilanka Telecom PLC',
    //             "statusList": [
    //                 { id: 0, title: 'Today - All', color: '#2874ed' },
    //                 { id: 1, title: 'This Week', color: '#00b542' },
    //                 { id: 2, title: 'This Month', color: '#8f45ff' }
    //             ],
    //             "ongoingTasksList": [
    //                 {
    //                     id: 0,
    //                     company: 'Sri Lanka Telecom - PLC',
    //                     taskName: 'Collect Cheques',
    //                     color: '#e06500'
    //                 },
    //             ],
    //             "taskList": [
    //                 {
    //                     id: 0,
    //                     company: 'Sri Lanka Telecom - PLC',
    //                     taskName: 'Collect Cheques',
    //                     time: '12.00am',
    //                     date: '17/09/2020',
    //                     color: '#e06500'
    //                 }
    //             ]
    //         },
    //         {
    //             id: 2,
    //             title: 'Dialog Axiata',
    //             "statusList": [
    //                 { id: 0, title: 'Today - All', color: '#2874ed' },
    //                 { id: 1, title: 'This Week', color: '#00b542' },
    //                 { id: 2, title: 'This Month', color: '#8f45ff' }
    //             ],
    //             "ongoingTasksList": [
    //                 {
    //                     id: 0,
    //                     company: 'Dialog Axiata PLC',
    //                     taskName: 'Collect Cheques',
    //                     color: '#2874ed'
    //                 },
    //             ],
    //             "taskList": [
    //                 {
    //                     id: 0,
    //                     company: 'Dialog Axiata PLC',
    //                     taskName: 'Collect Cheques',
    //                     time: '09.00am',
    //                     date: '17/09/2020',
    //                     color: '#2874ed'
    //                 }
    //             ]
    //         }
    //     ],
    // }

}