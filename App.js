/**
 * @author Shanilka
 */
import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Splash from "./src/screens/Splash";
import SignIn from "./src/screens/SignIn";
import Settings from "./src/screens/Settings";
import HomeManager from "./src/screens/HomeManager";
import TaskManager from "./src/screens/task/TaskManager";
import EditUserDetails from "./src/screens/registration/EditUserDetails";
import EditPaymentDetails from "./src/screens/registration/EditPaymentDetails";
import EditCompanyDetails from "./src/screens/registration/EditCompanyDetails";
import SelectWorkType from "./src/screens/SelectWorkType";

// import { createDrawerNavigator } from '@react-navigation/drawer';
// const Drawer = createDrawerNavigator();
// function DrawerHome() {
//   return (
//     <Drawer.Navigator initialRouteName="Home">
//       <Drawer.Screen name="Home" component={HomeManager} />
//     </Drawer.Navigator>
//   );
// }

const Stack = createStackNavigator();
export default class App extends Component {
  render() {
    return (
      <NavigationContainer independent={true}>
        <Stack.Navigator initialRouteName="SignIn">
          <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
          <Stack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} />
          <Stack.Screen name="Settings" component={Settings} options={{ headerShown: false }} />
          <Stack.Screen name="DrawerHome" component={HomeManager} options={{ headerShown: false }} />
          <Stack.Screen name="TaskManager" component={TaskManager} options={{ headerShown: false }} />

          <Stack.Screen name="SelectWorkType" component={SelectWorkType} options={{ headerShown: false }} />

          <Stack.Screen name="EditUserDetails" component={EditUserDetails} options={{ headerShown: false }} />
          <Stack.Screen name="EditCompanyDetails" component={EditCompanyDetails} options={{ headerShown: false }} />
          <Stack.Screen name="EditPaymentDetails" component={EditPaymentDetails} options={{ headerShown: false }} />


        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}